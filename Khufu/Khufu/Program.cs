﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cryptografy;

namespace Khufu
{
    class Program
    {
        static void Main(string[] args)
        {
            var key = "password";
            var text = "Hellow? World!";

            var w = Encrypt_ECB(text,key,1);
            Console.ReadKey();
        }
        public static string Encrypt_ECB(string text, string key, int oktateCount)
        {
            var textBlocks = text.Length % 8 == 0 ? text : text.PadLeft(text.Length + 8 - (text.Length % 8));
            var textBlocksBytes = CryptoFunctions.GetBytesFromText(textBlocks,8);
            var ShifrBytes = Encrypt_ECB(textBlocksBytes, key, (oktateCount >= 1 && oktateCount <= 8) ? oktateCount : 1);
            return CryptoFunctions.GetTextFromBytes(ShifrBytes);
        }
        private static byte[][] Encrypt_ECB(byte[][] text, string key, int oktateCount)
        {
            ExtendedKey(key);
            for (int i = 0; i < text.Length; i++)
            {
                for(int j=1; j<=8*oktateCount;j++)
                {
                    text[i] = KhufuRound(text[i], j % 8);
                }
            }
            return text;
        }
        private static byte[] KhufuRound(byte[] mainBlock, int round)
        {
            byte[] resultLeft = new byte[4];
            byte[] resultRight = new byte[4];
            var leftBlock = mainBlock.Take(4).ToArray();
            var rightBlock = mainBlock.Skip(4).ToArray();

            leftBlock = CryptoFunctions.XOR(new byte[][] { K0, leftBlock });
            rightBlock = CryptoFunctions.XOR(new byte[][] { K1, rightBlock });

            var q1 = CryptoFunctions.ExtendedPermutation(rulePermutaion, leftBlock[3]);
            var q2 = CryptoFunctions.XOR(new byte[][] { rightBlock, q1 });
            var q3 = CryptoFunctions.XOR(new byte[][] { K2, q2 });
            resultLeft = CryptoFunctions.XOR(new byte[][]{
                K2, CryptoFunctions.XOR(new byte[][]{
                    rightBlock, CryptoFunctions.ExtendedPermutation(rulePermutaion,leftBlock[3])
                })});
            var rotdist = 16;
            switch (round)
            {
                case 3 | 4: 
                    rotdist = 8; 
                    break;
                case 7 | 8:
                    rotdist = 24;
                    break;
            }
            resultRight = CryptoFunctions.XOR(new byte[][]{K3, CryptoFunctions.CycleShift(leftBlock,rotdist,false)});

            var result = resultLeft.Concat(resultRight).ToArray();

            return result;
        }
        private static void ExtendedKey(string key)
        {
            if(key.Length>64) key=Convert.ToString(key.Take(64));
            key = (key.Length < 64) ? key.PadLeft(64) : key;
            //var keyBytes = code.GetBytes(key);
            InitK();
            InitStartPermutation();
            var q = Encrypt_CBC(key,1);
        }
        public static string Encrypt_CBC(string text, int oktateCount)
        {
            var textBlocks = text.Length % 8 == 0 ? text : text.PadLeft(text.Length + 8 - (text.Length % 8));
            var textBlocksBytes = CryptoFunctions.GetBytesFromText(textBlocks, 8);
            var ShifrBytes = Encrypt_CBC(textBlocksBytes, (oktateCount >= 1 && oktateCount <= 8) ? oktateCount : 1);
            return CryptoFunctions.GetTextFromBytes(ShifrBytes);
        }
        private static byte[][] Encrypt_CBC(byte[][] text,int oktateCount)
        {
            text[0] = CryptoFunctions.XOR(new byte[][] { text[0], startVector });
            for (int j = 1; j <= 8 * oktateCount; j++)
            {
                text[0] = KhufuRound(text[0], j % 9);
            }

            for (int i = 1; i < text.Length; i++)
            {
                text[i] = CryptoFunctions.XOR(new byte[][] { text[i], text[i-1] });
                for (int j = 1; j <= 8 * oktateCount; j++)
                {
                    text[i] = KhufuRound(text[i], j % 9);
                }
            }
            return text;
        }
        private static void InitK()
        {
            K0 = new byte[4] { 0, 0, 0, 0 };
            K1 = new byte[4] { 0, 0, 0, 0 };
            K2 = new byte[4] { 0, 0, 0, 0 };
            K3 = new byte[4] { 0, 0, 0, 0 };
        }
        private static void InitStartPermutation()
        {
            for (int i = 0; i < 256; i++)
            {
                rulePermutaion[i] = new byte[] { (byte)i, (byte)i, (byte)i, (byte)i };
            }
        }
        static byte[] key;
        static byte[] K0 = new byte[4] { 0, 0, 0, 0 };
        static byte[] K1 = new byte[4] { 0, 0, 0, 0 };
        static byte[] K2 = new byte[4] { 0, 0, 0, 0 };
        static byte[] K3 = new byte[4] { 0, 0, 0, 0 };
        static byte[][] rulePermutaion = new byte[256][];
        static byte[] startVector = new byte[8]{0,0,0,0,0,0,0,0};
        static Encoding code = Encoding.Default;

    }
}
