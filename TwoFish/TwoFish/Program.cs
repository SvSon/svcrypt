﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ManyMonkeys.Cryptography;
using System.Security.Cryptography;
using System.Diagnostics;
using System.IO;

namespace TwoFish
{
    class Program
    {

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Выберете режим работы программы: ");
                Console.WriteLine("1. Шифрование");
                Console.WriteLine("2. Расшифрование");
                Console.WriteLine("3. Выход");
                var temp = Convert.ToInt16(Console.ReadLine());
                switch (temp)
                {
                    case 1:
                        Console.Clear();
                        string message = "";
                        using (StreamReader reader = new StreamReader(TextEncryptPath))
                        {
                            message = reader.ReadToEnd();
                        }
                        Console.WriteLine("Текст для зашифрования (взят из файла {1}): {0}", message, TextEncryptPath);
                        Console.WriteLine("Введите ключ шифрования: ");
                        var key = Console.ReadLine();
                        Console.WriteLine("---------------\n");
                        Console.WriteLine("Шифр (сохранен в файл {0}): ", TextDecryptPath);
                        var shifrText = Encrypt(message, key);
                        Console.WriteLine(shifrText);
                        using (StreamWriter writer = new StreamWriter(TextDecryptPath, false))
                        {
                            writer.Write(shifrText);
                        }
                        break;
                    case 2:
                        Console.Clear();
                        string shifr = "";
                        using (StreamReader reader = new StreamReader(TextDecryptPath))
                        {
                            shifr = reader.ReadToEnd();
                        }
                        Console.WriteLine("Текст для расшифрования (взят из файла {1}): {0}", shifr, TextDecryptPath);
                        Console.WriteLine("Введите ключ шифрования: ");
                        var keyDeshifr = Console.ReadLine();
                        Console.WriteLine("---------------\n");
                        Console.WriteLine("Шифр: ");
                        var deshifrText = Decrypt(shifr, keyDeshifr);
                        Console.WriteLine(deshifrText);
                        break;
                    case 3:
                        return;
                    default:
                        Console.Clear();
                        Console.WriteLine("Команда не определена");
                        break;
                }
                Console.ReadKey();
                Console.Clear();
            }
        }
        static string Encrypt(string text, string key)
        {
            Twofish fish = new Twofish();
            fish.Mode = CipherMode.ECB;
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            ICryptoTransform encode = new ToBase64Transform();
            var Key = KeyValidate(key);
            ICryptoTransform encrypt = fish.CreateEncryptor(Key, code.GetBytes(text)); // we use the plainText as the IV as in ECB mode the IV is not used

            CryptoStream cryptostreamEncode = new CryptoStream(ms, encode, CryptoStreamMode.Write);
            CryptoStream cryptostream = new CryptoStream(cryptostreamEncode, encrypt, CryptoStreamMode.Write);

            cryptostream.Write(code.GetBytes(text), 0, code.GetBytes(text).Length);
            cryptostream.Close();

            byte[] bytOut = ms.ToArray();
            return code.GetString(bytOut);
            
        }
        static string Decrypt(string shifr, string key)
        {
            Twofish fish = new Twofish();
            ICryptoTransform decode = new FromBase64Transform();
            var Key = KeyValidate(key);
            ICryptoTransform decrypt = fish.CreateDecryptor(Key, code.GetBytes(shifr));
            System.IO.MemoryStream msD = new System.IO.MemoryStream();
            CryptoStream cryptostreamD = new CryptoStream(msD, decrypt, CryptoStreamMode.Write);
            CryptoStream cryptostreamDecode = new CryptoStream(cryptostreamD, decode, CryptoStreamMode.Write);

            cryptostreamDecode.Write(code.GetBytes(shifr), 0, code.GetBytes(shifr).Length);
            cryptostreamDecode.Close();

            byte[] bytOutD = msD.ToArray(); 
            return code.GetString(bytOutD);
        }
        static byte[] KeyValidate(string key)
        {
            var keyValidate = key.Length < 32 ? key.PadLeft(32,' ') : key.Substring(0, 32);
            var byteKey = code.GetBytes(keyValidate);
            return byteKey;
        }
        
        static Encoding code = Encoding.Default;
        static string TextEncryptPath = "Text.txt";
        static string TextDecryptPath = "Shifr.txt";
    }
}
