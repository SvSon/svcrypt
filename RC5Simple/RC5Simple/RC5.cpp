#include <stdio.h>
#include <time.h>
#include "stdafx.h"
#include <stdlib.h>
#include <conio.h>

#include "RC5Simple.h"


int main()
{
	RC5Simple rc5(true);
	char key[RC5_B];
	char fileName[256];
	char tempFileName[256];
	vector<unsigned char> v_key(RC5_B);
  while (true)
            {
                printf("Enter program mode: \n");
                printf("1. Encrypt\n");
                printf("2. Decrypt\n");
                printf("3. Exit\n");
				int temp;
				temp=0;
                scanf("%x",&temp);
                switch (temp)
                {
                    case 1:
                        system("cls");
						printf("Enter key:\n");
						scanf("%s",key);
						for(int i=0; i<RC5_B; i++)
							v_key[i]=key[i];
						rc5.RC5_SetKey(v_key);
						printf("Enter name of file: \n");
						scanf("%s",fileName);
						for(int i=0;i<256;i++)
							tempFileName[i] = fileName[i];
						rc5.RC5_EncryptFile(fileName, strcat(tempFileName, ".encrypt"));
						printf("\nEncrypt file to %s\n",tempFileName);
                        break;
                    case 2:
                        system("cls");
						printf("Enter key\n");
						scanf("%s",key);
						for(int i=0; i<RC5_B; i++)
							v_key[i]=key[i];
						 rc5.RC5_SetKey(v_key);
						 printf("Enter name of file (without \".encrypt\"): \n");
						scanf("%s",fileName);
						for(int i=0;i<256;i++)
							tempFileName[i] = fileName[i];
						rc5.RC5_DecryptFile(strcat(fileName, ".encrypt"), strcat(tempFileName, ".decrypt"));
						printf("\nDecrypt file to %s\n",tempFileName);
                        break;
                    case 3:
                        return 0;
                    default:
                        system("cls");
                        printf("Invalid command!\n");
                        break;
                }
                getch();
                system("cls");
            }
  return 0;
}
