﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cryptografy;

namespace Lucifer
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Введите текст: ");
            var message = "Hellow world";//Console.ReadLine();
            //Console.WriteLine("Введите ключ шифрования: ");
            var key = "12345678";//Console.ReadLine();
            Console.WriteLine("---------------\n");
            Console.WriteLine("Шифр: ");
            Console.WriteLine(Encrypt(message, key));

            Console.ReadKey();
        }

        public static string Encrypt(string text, string password)
        {
            InitKey(password);
            var textByte = CryptoFunctions.GetBytesFromText(text,4);
            for (int j = 0; j < textByte.Length; j++)
            {
                for (int i = 0; i < 15; i++)
                {
                    var x = LuciferRound(textByte[j], i, true);
                    textByte[j] = x;
                }
                var _x = LuciferRound(textByte[j], 15, false);
                textByte[j] = _x;
            }
            return CryptoFunctions.GetTextFromBytes(textByte);
        }
        private static byte[] LuciferRound(byte[] block, int roundNumber,bool Isextend)
        {
            byte[] result = new byte[block.Length];
            var blockA = block.Take(2).ToArray();
            var blockB = block.Skip(2).ToArray();

            var blockABin4 = FromByteToBin4(blockA);
            var blockBBin4 = FromByteToBin4(blockB);
            byte[] resultBBin4 = new byte[blockBBin4.Length];

            for (int i = 0; i < blockABin4.Length; i++)
            {
                blockABin4[i] = (byte)CryptoFunctions.MOD(blockABin4[i] + Key[KAi[i][roundNumber]], 16);
                var Tfromblock = T[blockABin4[i]];
                int indexB=0;
                if (CryptoFunctions.GetBinFromBytesBlock((new byte[] { Key[KXi[i][roundNumber]] })).Substring(4, 4)[i] == '1')
                {
                    indexB = (roundNumber + i + 1) % 4;
                }
                else
                {
                    indexB = (roundNumber + i) % 4;

                }
                var B = CryptoFunctions.GetBinFromBytesBlock(new byte[] { blockBBin4[i] }).Substring(4, 4)[indexB];
                Tfromblock = CryptoFunctions.XOR(Tfromblock, B.ToString());
                resultBBin4[i] = Convert.ToByte(Tfromblock, 2);
            }

            byte[] resultA = blockA;
            byte[] resultB = FromBin4ToByte(resultBBin4);

            if (Isextend)
            {
                for (int i = 0; i < resultA.Length; i++)
                {
                    result[i] = resultB[i];
                    result[i + resultA.Length] = resultA[i];
                }
            }
            else
            {
                for (int i = 0; i < resultA.Length; i++)
                {
                    result[i] = resultA[i];
                    result[i + resultA.Length] = resultB[i];

                }
            }

            return result;

        }

        private static void InitKey(string key)
        {
            if (key.Length < 8) key = key.PadLeft(8);
            if (key.Length > 8) key = key.Substring(0, 8);
            var keyBytes = code.GetBytes(key);
            Key = FromByteToBin4(keyBytes);
            InitT();
        }
        private static byte[] FromByteToBin4(byte[] value)
        {
            byte[] result = new byte[value.Length * 2];
            for (int i = 0; i < value.Length; i++)
            {
                var keyBunString = CryptoFunctions.GetBinFromBytesBlock(new byte[] { value[i] });
                var left = Convert.ToByte(keyBunString.Substring(0, 4), 2);
                var right = Convert.ToByte(keyBunString.Substring(4, 4), 2);
                result[2 * i] = left;
                result[2 * i + 1] = right;
            }
            return result;
        }
        private static byte[] FromBin4ToByte(byte[] value)
        {
            byte[] result = new byte[value.Length / 2];
            for (int i = 0; i < value.Length; i+=2)
            {
                var leftBlock = CryptoFunctions.GetBinFromBytesBlock(new byte[] { value[i] });
                var rightBlock = CryptoFunctions.GetBinFromBytesBlock(new byte[] { value[i+1] });
                result[i/2] = Convert.ToByte(leftBlock.Substring(4, 4) + rightBlock.Substring(4, 4), 2);
            }
            return result;
        }
        private static void InitT()
        {
            //i - номер раунда
            for (int i = 0; i < 16; i++)
            {
                string temp = "";
                for (int j = 0; j < 4; j++)
                {
                    temp += GetBitFromT(i, j);
                }
                T[i] = temp;
            }
        }
        private static string GetBitFromT(int i, int j)
        {
            switch (TMask[i][j])
            {
                case '0': return "0";
                case '1': return "1";
                case 'x': return CryptoFunctions.GetBinFromBytesBlock(new byte[] { Key[KSi[i]] }).Substring(4, 4)[j].ToString();
                case 'y': return CryptoFunctions.GetBinFromBytesBlock(new byte[] { Key[KSi[i]] }).Substring(4, 4)[j] == '0' ? "1" : "0";
                default: return "";
            }
        }

        #region Ключи...
        static byte[] Key = new byte[16];
        static int[][] KAi = new int[][]{ 
            new int[]{ 1, 10, 3, 12, 5, 14, 7, 0, 9, 2, 11, 4, 13, 6, 15, 8 },
            new int[]{ 3, 12, 5, 14, 7, 0, 9, 2, 11, 4, 13, 6, 15, 8, 1, 10 },
            new int[]{ 5, 14, 7, 0, 9, 2, 11, 4, 13, 6, 15, 8, 1, 10, 3, 12 },
            new int[]{ 7, 0, 9, 2, 11, 4, 13, 6, 15, 8, 1, 10, 3, 12, 5, 14 }};

        static int[] KSi = new int[] { 0, 9, 2, 11, 4, 13, 6, 15, 8, 1, 10, 3, 12, 5, 14, 7 };
        static int[][] KXi = new int[][]{ 
            new int[] { 2, 11, 4, 13, 6, 15, 8, 1, 10, 3, 12, 5, 14, 7, 0, 9 },
            new int[] { 4, 13, 6, 15, 8, 1, 10, 3, 12, 5, 14, 7, 0, 9, 2, 11 },
            new int[] { 6, 15, 8, 1, 10, 3, 12, 5, 14, 7, 0, 9, 2, 11, 4, 13 },
            new int[] { 8, 1, 10, 3, 12, 5, 14, 7, 0, 9, 2, 11, 4, 13, 6, 15 }};
        #endregion
        #region Таблицы замен...
        private static string[] T = new string[16];
        private static string[] TMask = new string[]{"y1xx","yy1y","x11y","10yx","yy1x","1yx1","y01y","0x00","xxy0","x1yx","00y1","x0xy","yxxy","x0xx","xy0y","yx0x"};
        #endregion

        static Encoding code = Encoding.Default;
    }
}
