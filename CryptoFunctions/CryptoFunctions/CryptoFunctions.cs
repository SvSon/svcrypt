﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cryptografy
{
    public class CryptoFunctions
    {
        #region XOR //Сложение по модулю 2...
        public static string XOR(params string[] args)
        {
            if (args.Length == 0) return null;
            if (args.Length == 1) return args[0];
            var maxLength = args.Max(item => item.Length);
            var arguments = args.Select(elem => elem.PadLeft(maxLength, Convert.ToChar(0))).ToArray();
            string result = "";
            for (int i = 0; i < maxLength; i++)
            {
                result += arguments.Where(elem => elem[i] == '1').Count() % 2 == 0 ? '0' : '1';
            }
            return result;
        }
        public static byte[] XOR(params byte[][] args)
        {
            string[] array = new string[args.Length];
            for(int i=0;i<args.Length;i++)
            {
                array[i] += GetBinFromBytesBlock(args[i]);
            }
            var arrayResultBytes = GetBytesFromBin(XOR(array), 8);
            return arrayResultBytes;
        }
        public static byte XOR(params byte[] args)
        {
            var q = XOR(args.Select(elem => Convert.ToString(elem, 2)).ToArray());
            var w = Convert.ToInt32(q, 2);
            var e = (byte)w;
            return e;
        }


        #endregion
        #region Двоичные преобразования...
        public static string GetBinFromBytesBlock(byte[] value)
        {
            string result=String.Empty;
            foreach (var item in value)
                result += Convert.ToString(item, 2).PadLeft(8, '0');
            return result;
        }
        #endregion
        #region MOD //Взятие по модулю...
        public static ulong MOD(ulong x, ulong mod)
        {
            return x % mod;
        }
        public static int MOD(int x, int mod)
        {
            return x % mod;
        }
        public static string MOD(string x, string mod)
        {
            return Convert.ToString(Convert.ToInt64(x, 2) % Convert.ToInt64(mod, 2), 2);
        }
        public static string MOD(string x, long mod)
        {
            return Convert.ToString(Convert.ToInt64(x, 2) % mod, 2);
        }
        public static string MOD(long x, string mod)
        {
            return Convert.ToString(x % Convert.ToInt64(mod, 2), 2);
        }
        #endregion
        #region  PowModule //Возведение в степень по модулю...
        public static ulong PowModule(ulong value, int power, ulong mod = 1)
        {
            if (mod == 0) return 0;
            if (mod == 1) return (ulong)Math.Pow(value, power);
            if (power == 128) return 0;
            ulong result = 1;
            for (int i = 0; i < power; i++)
                result = MOD(result * value, mod);
            return result; ;
        }
        //public static ulong PowModule(byte[] value, int power, ulong mod = 1)
        //{
        //    if (mod == 0) return 0;
        //    if (mod == 1) return (ulong)Math.Pow(value, power);
        //    if (power == 128) return 0;
        //    ulong result = 1;
        //    for (int i = 0; i < power; i++)
        //        result = MOD(result * value, mod);
        //    return result; ;
        //}

        #endregion
        #region  LogModule //Взятие логарифма по модулю по модулю...
        public static ulong LogModule(ulong value, ulong newBase, ulong mod = 1)
        {
            if (mod == 0) return 0;
            if (value == 0) return 128;
            for (ulong i = 0; i < mod; i++)
            {
                if (PowModule(newBase, (int)i, mod) == value) return i;
            }
            return 0;
        }
        //public static ulong PowModule(byte[] value, int power, ulong mod = 1)
        //{
        //    if (mod == 0) return 0;
        //    if (mod == 1) return (ulong)Math.Pow(value, power);
        //    if (power == 128) return 0;
        //    ulong result = 1;
        //    for (int i = 0; i < power; i++)
        //        result = MOD(result * value, mod);
        //    return result; ;
        //}

        #endregion
        #region Permutation //Перестановка по правилу...
        /// <summary>
        /// Перестановка по правилу
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="rule">Правило</param>
        /// <returns></returns>
        public static string Permutation(string value, string rulePermutation)
        {
            string result = "";
            var rule = rulePermutation.Split(new char[]{' '},StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < rule.Length; i++)
            {
                result += value[Int32.Parse(rule[i].ToString()) - 1];
            }
            return result;
        }
        public static byte[] Permutation(byte[] value, string rule)
        {
            return code.GetBytes(Permutation(code.GetString(value),rule));
        }
        #endregion
        #region Псевдоадамаровые преобразования...
        public static byte[] PHT(byte value1, byte value2)
        {
            return new byte[] { (byte)MOD(Convert.ToUInt64(2 * value1 + value2), 256), (byte)MOD(Convert.ToUInt64(value1 + value2), 256) };
        }
        public static byte[] IPHT(byte value1, byte value2)
        {
            return new byte[] { 
                (byte)MOD(Convert.ToUInt64(subtractionModule(256,value1, value2)), 256), 
                (byte)MOD(Convert.ToUInt64(subtractionModule(256,2*value2, value1)), 256) 
            };
        }
        #endregion
        #region Побитовый cдвиг...
        public static string CycleShift(string value, int rotdist, bool IsRight)
        {
            int n = value.Length;
            if (rotdist == 0 || rotdist == n) return value;
            if (IsRight) return value.Substring(n - rotdist, rotdist) + value.Substring(0, n - rotdist);
            else return value.Substring(rotdist % n, n - rotdist % n) + value.Substring(0, rotdist % n);
        }
        public static byte[] CycleShift(byte[] value, int rotdist, bool IsRight)
        {
            return GetBytesFromBin(CycleShift(GetBinFromBytesBlock(value), rotdist, IsRight), 8);
        }

        #endregion
        #region Сложение по модулю...
        public static byte AddModule(int module, params byte[] args)
        {
            int sum = 0;
            foreach (var elem in args)
            {
                var w = MOD(elem, module);
                var e = (byte)w;
                sum += e;
            }
            var r = (byte)MOD(sum, module);
            return r;
        }
        public static byte[] AddModule(int module, params byte[][] args)
        {
            return AddModule(module, args.Select(item => code.GetString(item)).ToArray());
        }
        public static byte[] AddModule(int module, params string[] args)
        {
            if (args.Length == 0) return null;
            if (args.Length == 1) return code.GetBytes(args[0]);
            var maxLength = args.Max(item => item.Length);
            var arguments = args.Select(elem => elem.PadLeft(maxLength, Convert.ToChar(0)));
            byte[] result = new byte[maxLength];
            for (int i = 0; i < maxLength; i++)
            {
                int sum = 0;
                foreach (var elem in arguments)
                {
                    var q = Convert.ToInt32(elem[i]);
                    var w = MOD(q, module);
                    var e = (byte)w;
                    sum += e;
                }
                result[i] = (byte)MOD(sum,module);
            }
            return result;
        }
        #endregion
        #region Разность по модулю...
        /// <summary>
        /// Вычитание по модулю
        /// </summary>
        /// <param name="module">Модуль</param>
        /// <param name="value1">Уменьшаемое</param>
        /// <param name="value2">Вычитаемое</param>
        /// <returns></returns>
        public static byte subtractionModule(int module, int value1, int value2)
        {
            int sum = value1 - value2;
            if (sum < 0)
            {
                var q = sum + ((-1) * sum / module) * module;
                sum = q;
            }
            return (byte)sum;
        }
        #endregion
        #region Расширяющая таблица замен...
        /// <summary>
        /// Расширяющая замена символа по правилу
        /// </summary>
        /// <param name="rule">Правило, по которому осуществляется замена</param>
        /// <param name="value">Входное значение символа</param>
        /// <returns></returns>
        public static byte[] ExtendedPermutation(byte[][] rule, byte value)
        {
            return rule[value % rule.Length];
        }
        #endregion
        #region Разбиение на блоки и слияние в строку...
        public static byte[][] GetBytesFromText(string text, int blockLength)
        {
            var n = text.Length;
            int temp = 0;
            while (true)
            {
                if ((n + temp) % blockLength != 0)
                {
                    temp++;
                }
                else break;
            }
            var newTextPad = text.PadLeft(n + temp, Convert.ToChar(0));
            byte[][] result = new byte[newTextPad.Length / blockLength][];
            for (int i = 0; i < newTextPad.Length; i += blockLength)
            {
                var q = newTextPad.Substring(i, blockLength);
                result[i / blockLength] = code.GetBytes(q);
            }
            return result;
        }
        public static string GetTextFromBytes(byte[][] textBytes)
        {
            var n = textBytes.Length;
            string result = "";
            for (int i = 0; i < textBytes.Length; i++)
            {
                result += code.GetString(textBytes[i]);
            }
            return result;
        }
        public static byte[] GetBytesFromBin(string text, int blockLength)
        {
            var n = text.Length;
            int temp = 0;
            while (true)
            {
                if ((n + temp) % blockLength != 0)
                {
                    temp++;
                }
                else break;
            }
            var newTextPad = text.PadLeft(n + temp, Convert.ToChar(0));
            byte[] result = new byte[newTextPad.Length / blockLength];
            for (int i = 0; i < newTextPad.Length; i += blockLength)
            {
                var q = newTextPad.Substring(i, blockLength);
                result[i / blockLength] = Convert.ToByte(q,2);
            }
            return result;
        }
        #endregion
        static Encoding code = Encoding.Default;
    }
}
