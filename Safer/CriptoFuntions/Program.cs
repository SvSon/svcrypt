﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cryptografy;
using System.IO;

namespace SaferShifr
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Выберете режим работы программы: ");
                Console.WriteLine("1. Шифрование");
                Console.WriteLine("2. Расшифрование");
                Console.WriteLine("3. Выход");
                var temp = Convert.ToInt16(Console.ReadLine());
                switch (temp)
                {
                    case 1:
                        Console.Clear();
                        string message = "";
                        using(StreamReader reader = new StreamReader(TextEncryptPath) )
	                    {
		                    message = reader.ReadToEnd();
	                    }
                        Console.WriteLine("Текст для зашифрования (взят из файла {1}): {0}", message, TextEncryptPath);
                        Console.WriteLine("Введите ключ шифрования: ");
                        var key = Console.ReadLine();
                        Console.WriteLine("Введите количество раундов шифрования: ");
                        var roundCount = Convert.ToInt16(Console.ReadLine());
                        Console.WriteLine("---------------\n");
                        Console.WriteLine("Шифр (сохранен в файл {0}): ", TextDecryptPath);
                        var shifrText = SaferEncryptions(message, key, roundCount);
                        Console.WriteLine(shifrText);
                        using (StreamWriter writer = new StreamWriter(TextDecryptPath, false))
                        {
                            writer.Write(shifrText);
                        }
                        break;
                    case 2:
                        Console.Clear();
                        string shifr = "";
                        using(StreamReader reader = new StreamReader(TextDecryptPath) )
	                    {
                            shifr = reader.ReadToEnd();
	                    }
                        Console.WriteLine("Текст для расшифрования (взят из файла {1}): {0}", shifr, TextDecryptPath);
                        Console.WriteLine("Введите ключ шифрования: ");
                        var keyDeshifr = Console.ReadLine();
                        Console.WriteLine("Введите количество раундов шифрования: ");
                        var roundDeshifrCount = Convert.ToInt16(Console.ReadLine());
                        Console.WriteLine("---------------\n");
                        Console.WriteLine("Шифр: ");
                        var deshifrText = SaferDecryptions(shifr, keyDeshifr, roundDeshifrCount);
                        Console.WriteLine(deshifrText);
                        break;
                    case 3:
                        return;
                    default:
                        Console.Clear();
                        Console.WriteLine("Команда не определена");
                        break;
                }
                Console.ReadKey();
                Console.Clear();
            }
            //Console.WriteLine("Введите текст: ");
            //var message = "SAFERING ololore sdlks dsj o kjl !8977кк";
            //Console.WriteLine(message);
            //Console.WriteLine("Шифр: ");
            //var shifr = SaferEncryptions(message, "password", 10);
            //Console.WriteLine(shifr);
            //Console.WriteLine("Расшифровка: ");
            //Console.WriteLine(SaferDecryptions(shifr, "password", 10));
            //Console.ReadKey();
        }
        /// <summary>
        /// Алгоритм расширения ключей
        /// </summary>
        /// <param name="StartKey">Исходный ключ шифрования</param>
        /// <param name="numberOfRounds">Количество раундов</param>
        /// <returns>Массив 8-ми байтовых ключей</returns>
        public static byte[][] ExtensionKey_Kx64(byte[] StartKey, int numberOfRounds)
        {
            if (numberOfRounds < 2) return new byte[][] { StartKey };
            int n = 2 * numberOfRounds + 1;
            byte[][] Keys = new byte[n][];
            for (int i = 0; i < n; i++)
                Keys[i] = new byte[8];
            byte[][] Constants = GetConstants(numberOfRounds);
            Keys[0]=StartKey;
            for (int i = 1; i < n; i++)
            {
                var q = CryptoFunctions.CycleShift(Keys[i-1], 3, false);
                Keys[i] = CryptoFunctions.AddModule(256,q,Constants[i]);
            }
            return Keys;
        }
        /// <summary>
        /// Вычисление констант для генерации ключей
        /// </summary>
        /// <param name="numberOfRounds">Количество раундов</param>
        /// <returns>Массив 8-ми байтовых констант</returns>
        public static byte[][] GetConstants(int numberOfRounds)
        {
            int n = 2 * numberOfRounds + 1;
            byte[][] B = new byte[n][];
            for (int i = 0; i < n; i++)
                B[i] = new byte[8];

            for (int i = 0; i < n; i++)
            {
                byte[] mas = new byte[8];
                for (int j = 1; j < 9; j++)
                {
                    var value = 9 * (i+1) + j;
                    var E1 = (int)CryptoFunctions.PowModule(45, value, 257);
                    var E2 = (byte)CryptoFunctions.PowModule(45, E1, 257);
                    var c = Convert.ToChar(E2);
                    mas[j - 1] = E2;
                }
                B[i] = mas;
            }

            return B;
        }
        public static byte[] roundPHT(byte[] value,Func<byte, byte ,byte[]> function)
        {
            int n = value.Length;
            if(n % 2 != 0) return null;
            byte[] result = new byte[n];
            for (int i = 0; i < n-1; i+=2)
            {
                var temp = function(value[i], value[i + 1]);
                result[i] = temp[0];
                result[i+1] = temp[1];
            }
            return result;
        }
        /// <summary>
        /// Раунд шифрования
        /// </summary>
        /// <param name="text">Шифруемый блок текста</param>
        /// <param name="keys">Весь набор ключей</param>
        /// <param name="round">Номер раунда</param>
        /// <returns></returns>
        public static byte[] SaferEncryptionsRound(byte[] text, byte[][] keys, int round)
        {
            int n = text.Length;
            int roundKey = 2*(round)-2;
            byte[] temp = new byte[text.Length];// = text;
            for (int i = 0; i < n; i++)
            {
                if ((new int[] { 0, 3, 4, 7 }).Contains(i))
                {
                    temp[i] = CryptoFunctions.XOR(text[i], keys[roundKey][i]);
                }
                else
                {
                    temp[i] = CryptoFunctions.AddModule(256,text[i],keys[roundKey][i]);
                }
            }
            ////////

            for (int i = 0; i < n; i++)
            {
                if ((new int[] { 0, 3, 4, 7 }).Contains(i))
                {
                    temp[i] = (byte)CryptoFunctions.PowModule(45, temp[i], 257);
                }
                else
                {
                    temp[i] = (byte)CryptoFunctions.LogModule(temp[i],45,257);
                }
            }
            ///////
            for (int i = 0; i < n; i++)
            {
                if (!(new int[] { 0, 3, 4, 7 }).Contains(i))
                {
                    temp[i] = CryptoFunctions.XOR(temp[i], keys[roundKey + 1][i]);
                }
                else
                {
                    temp[i] = CryptoFunctions.AddModule(256, temp[i], keys[roundKey + 1][i]);
                }
            }

            temp = roundPHT(temp,CryptoFunctions.PHT);
            temp = CryptoFunctions.Permutation(temp, rulePermutation);
            temp = roundPHT(temp, CryptoFunctions.PHT);
            temp = CryptoFunctions.Permutation(temp, rulePermutation);
            temp = roundPHT(temp, CryptoFunctions.PHT);

            return temp;
        }
        public static string SaferEncryptions(string text, string password, int roundCount)
        {
            if (password.Length < 8) password = password.PadLeft(8, Convert.ToChar(0));
            if (password.Length > 8) password = password.Substring(0, 8);
            var rev = ExtensionKey_Kx64(code.GetBytes(password), roundCount);//генерация ключей
            var textByte = GetBytesFromText(text);
            for (int j = 0; j < textByte.Length; j++)
            {
                for (int i = 1; i < roundCount; i++)
                {
                    var x = SaferEncryptionsRound(textByte[j], rev, i);
                    textByte[j] = x;
                }
            }
            return GetTextFromBytes(textByte);
        }
        public static string SaferDecryptions(string text, string password, int roundCount)
        {
            if (password.Length < 8) password = password.PadLeft(8, Convert.ToChar(0));
            if (password.Length > 8) password = password.Substring(0,8);
            var rev = ExtensionKey_Kx64(code.GetBytes(password), roundCount);//генерация ключей
            var textByte = GetBytesFromText(text);
            for (int j = 0; j < textByte.Length; j++)
            {
                for (int i = 1; i < roundCount; i++)
                {
                    var x = SaferDecryptionsRound(textByte[j], rev, i, roundCount-1);
                    textByte[j] = x;
                }
            }
            return GetTextFromBytes(textByte);
        }
        /// <summary>
        /// Дешифрование
        /// </summary>
        /// <param name="shifrText">Дешифруемый текст</param>
        /// <param name="keys">Набор ключей</param>
        /// <param name="round">текущий раунд</param>
        /// <param name="roundsCount">количество раундов</param>
        /// <returns></returns>
        public static byte[] SaferDecryptionsRound(byte[] shifrText, byte[][] keys, int round,int roundsCount)
        {
            int n = shifrText.Length;
            int roundKey = 2 * (roundsCount) + 1 - 2 * round;
            byte[] temp = new byte[shifrText.Length];// = text;

            temp = roundPHT(shifrText, CryptoFunctions.IPHT);
            temp = CryptoFunctions.Permutation(temp, rulePermutationReturn);
            temp = roundPHT(temp, CryptoFunctions.IPHT);
            temp = CryptoFunctions.Permutation(temp, rulePermutationReturn);
            temp = roundPHT(temp, CryptoFunctions.IPHT);

            for (int i = 0; i < n; i++)
            {
                if (!(new int[] { 0, 3, 4, 7 }).Contains(i))
                {
                    temp[i] = CryptoFunctions.XOR(temp[i], keys[roundKey][i]);
                }
                else
                {
                    temp[i] = CryptoFunctions.subtractionModule(256, temp[i], keys[roundKey][i]);
                }
            }
            ////////

            for (int i = 0; i < n; i++)
            {
                if (!(new int[] { 0, 3, 4, 7 }).Contains(i))
                {
                    temp[i] = (byte)CryptoFunctions.PowModule(45, temp[i], 257);
                }
                else
                {
                    temp[i] = (byte)CryptoFunctions.LogModule(temp[i], 45, 257);
                }
            }
            ///////
            for (int i = 0; i < n; i++)
            {
                if ((new int[] { 0, 3, 4, 7 }).Contains(i))
                {
                    temp[i] = CryptoFunctions.XOR(temp[i], keys[roundKey - 1][i]);
                }
                else
                {
                    temp[i] = CryptoFunctions.subtractionModule(256, temp[i], keys[roundKey - 1][i]);
                }
            }

            return temp;
        }
        public static byte[][] GetBytesFromText(string text)
        {
            var n = text.Length;
            int temp = 0;
            while (true)
            {
                if ((n+temp) % 8 != 0)
                {
                    temp++;
                }
                else break;
            }
            var newTextPad = text.PadLeft(n + temp, Convert.ToChar(0));
            byte[][] result = new byte[newTextPad.Length / 8][];
            for (int i = 0; i < newTextPad.Length; i += 8)
            {
                var q = newTextPad.Substring(i, 8);
                result[i / 8] = code.GetBytes(q);
            }
            return result;
        }
        public static string GetTextFromBytes(byte[][] textBytes)
        {
            var n = textBytes.Length;
            string result = "";
            for (int i = 0; i < textBytes.Length; i++)
            {
                result += code.GetString(textBytes[i]);
            }
            return result;
        }
        
        #region Поля...
        static Encoding code = Encoding.Default;
        static string rulePermutation = "1 3 5 7 2 4 6 8";
        static string rulePermutationReturn = "1 5 2 6 3 7 4 8";
        static string TextEncryptPath = "Text.txt";
        static string TextDecryptPath = "Shifr.txt";
        #endregion
    }
}
