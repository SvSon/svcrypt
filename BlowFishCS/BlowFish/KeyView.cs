﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BlowFish
{
    public partial class KeyView : Form
    {
        public KeyView(string key = "")
        {
            InitializeComponent();
            Key = key;
        }
        public string Key { get; set; }

        private void button1_Click(object sender, EventArgs e)
        {
            Key = textBox1.Text;
            this.Close();
        }
    }
}
