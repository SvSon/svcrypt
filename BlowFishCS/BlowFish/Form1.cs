﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BlowFishCS;

namespace BlowFish
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ключШифрованияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (KeyView keyProperties = new KeyView(key == null ? "" : Encoding.GetString(key)))
            {
                keyProperties.ShowDialog();
                key = keyProperties.Key == null ? key : Encoding.GetBytes(keyProperties.Key);
                var q = Encoding.GetString(key);
            }
        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {
            if (key == null)
            {
                MessageBox.Show("Невозможно выполнить операции с пустым ключем!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var Cryptor = new BlowFishCS.BlowFish(key);
            string plainText = textBox1.Text;
            string cipherText = Cryptor.Encrypt_CBC(plainText);
            textBox2.Text = Encoding.GetString(Cryptor.HexToByte(cipherText)); ;
        }

        byte[] key;
        Encoding Encoding = Encoding.Default;

        private void toolStripStatusLabel2_Click(object sender, EventArgs e)
        {
            if (key == null)
            {
                MessageBox.Show("Невозможно выполнить операции с пустым ключем!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var Decryptor = new BlowFishCS.BlowFish(key);
            string plainText = textBox1.Text;
            //if (plainText.Length <= 16)
            //{
            //    MessageBox.Show("Шифртекст не может содержать меньше 16 символов!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}
            var cipherText = Decryptor.Decrypt_CBC(Decryptor.ByteToHex(Encoding.GetBytes(plainText)));
            textBox2.Text = cipherText;
        }

        private void закрытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
